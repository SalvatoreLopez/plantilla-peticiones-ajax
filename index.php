<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Inicio</title>
    <!-- CSS  de foundation -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/css/foundation.min.css" />
    <!-- lbreria motion para modals e inputs -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/motion-ui/1.1.1/motion-ui.min.css" />
</head>
<body>
    <main>
    
        <!--tabs de opciones -->
        <ul class="tabs" data-tabs id="example-tabs">
            <li class="tabs-title is-active"><a  id="verInformacion" class="expanded" href="#panel1" aria-selected="true">Ver información</a></li>
            <li class="tabs-title"><a class="expanded"  href="#panel2">Agregar Información</a></li>
        </ul>
        <!--tabs de opciones -->
        <!--contenido de los tabs -->
        <div class="tabs-content" data-tabs-content="example-tabs">
            <div class="tabs-panel is-active" id="panel1">
                <table>
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Contraseña</th>
                            <th>Tipo</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody id="tBody"></tbody>
                </table>
                <!-- modal para editar usuario -->
                <div class="reveal" id="modal" data-reveal data-close-on-click="true" data-animation-in="spin-in" data-animation-out="spin-out">
                    <h1>Editar Usuario</h1>
                    <form action="" id="formularioActualizarDatos">
                        <input type="number"  name ="idUsuario" id="idUsuario">
                        <label >Usuario</label>
                        <input type="text" name ="nombre" id="nombreEditar">
                        <label >Contraseña</label>
                        <input type="text" name ="password" id="passwordEditar">
                        <label >Tipo de Usuario</label>
                        <select name="" id="tipoUsuarioEditar">
                            <option value="0">Elige Usuario</option>
                            <option value="1">Administrador</option>
                            <option value="2">Usuario</option>
                        </select>
                        <button class="button">Guardar datos</button>
                    </form>
                    <button class="close-button" data-close aria-label="Close reveal" type="button">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- modal para editar usuario -->
            </div>
            <div class="tabs-panel" id="panel2">
                <form action="" id="formularioAgregarUsuario">
                    <input type="text" name="usuario" id="usuario" required>
                    <input type="password" name="password" id="password" required>
                    <select name="opcionUsuario" id="opcionUsuario" required>
                        <option value="0">Elige usuario</option>
                        <option value="1">Administrador</option>
                        <option value="2">Usuario</option>
                    </select>
                     <button class="button" >Agregar Usuario</button>
                </form>
            </div>
        </div>
        <!--contenido de los tabs -->
        <!-- jquery -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
        <!-- JavaScript de foundation -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.4.3/js/foundation.min.js" ></script>
        <!-- librería personalizada -->
        <script src="js/app.js"></script>
    </main> 
</body>
</html>