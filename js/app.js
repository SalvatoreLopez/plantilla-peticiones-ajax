//inicializa el plugin de foundation
$(document).foundation();
//función que cargara todas las demás funciones una vez que la pagina este completamente cargada
$(window).ready(() => {
    cargarDatos();

    $('#verInformacion').click(() => {
        cargarDatos();
    });

    $('#formularioAgregarUsuario').submit((e) => {
        e.preventDefault();
        let objetoCrearUsuario = {
            usuario: this.usuario.value.trim(),
            password: this.password.value.trim(),
            tipoUsuario: $('#opcionUsuario').val(),
            tipo : "agregarInformacion"

        };
        if (!isEmpty(objetoCrearUsuario.usuario) && !isEmpty(objetoCrearUsuario.password) && !isEmpty(objetoCrearUsuario.tipoUsuario)){
            agregarInformacion(objetoCrearUsuario)    
       }

    });

    $('#formularioActualizarDatos').submit((e) => {
        let objetoEditarUsuario = {
            idUsuario: this.idUsuario.value.trim(),
            usuario: this.nombreEditar.value.trim(),
            password: this.passwordEditar.value.trim(),
            tipoUsuario: $('#tipoUsuarioEditar').val(),
            tipo: "guardarInformacion"
        };
        actualizarInfotmacion(objetoEditarUsuario)
    });
});


//verifica si los input están vacíos
var isEmpty = (input) => {
    return (input.length === 0) ? true : false;
}

var cargarDatos = () => {
    $.ajax({
        data: 'tipo=cargarInformacion',
        url: 'php/opciones-a-realizar.php',
        type: "post",
        beforeSend: () => {
            console.log("Procesando información")
        },
        complete: () => {
            console.log("se termino de procesar la información")
        },
        success: (respuesta) => {
            if (respuesta.estado == 'ok') {
                $("#tBody > tr").remove();
                $.each(respuesta.mensaje,(indice,valor) => {
                    $("#tBody").append('<tr><td>' + valor.nombre + '</td><td>' + valor.contrasenia + '</td><td>' + valor.tipo + '</td><td><div class="button-group"><button onclick="eliminar(' + valor.idUsuario + ')" class="button alert">Eliminar</button><button class="button" data-toggle="modal" onclick="editarUsuario(' + valor.idUsuario + ')">Editar</button></div></td></tr>');
                })
            }
            else {
                alert(respuesta.mensaje)
            }
        }
    });
}

var agregarInformacion = (objetoUsuario) => {
    $.ajax({
        data: objetoUsuario,
        url: 'php/opciones-a-realizar.php',
        type: "post",
        beforeSend: () => {
            console.log("Procesando información")
        },
        complete: () => {
            console.log("se termino de procesar la información")
        },
        success: (respuesta) => {
            if (respuesta.estado == 'ok') {
                alert(respuesta.mensaje);
              
            }
            else {
                alert(respuesta.mensaje)
            }
        }
    });
    $("#formularioAgregarUsuario")[0].reset();
}

var eliminar = (idUsuario) => {
    if (confirm('¿Estás seguro de eliminar el usuario?'))
        $.ajax({
            data: { idUsuario: idUsuario , tipo: 'eliminarUsuario' },
            url: 'php/opciones-a-realizar.php',
            type: "post",
            beforeSend: () => {
                console.log("Procesando información")
            },
            complete: () => {
                console.log("se termino de procesar la información")
            },
            success: (respuesta) => {
                if (respuesta.estado == 'ok') {
                    alert(respuesta.mensaje)
                    cargarDatos();
                }
                else {
                    alert(respuesta.mensaje)
                }
            }
        });
}

var editarUsuario = (idUsuario) => {
    $('#idUsuario').val(idUsuario)
    $("input#idUsuario").attr('disabled', 'disabled');
    $.ajax({
        data: { idUsuario : idUsuario , tipo: 'informacionUsuario' },
        url: 'php/opciones-a-realizar.php',
        type: "post",
        beforeSend: () => {
            console.log("Procesando información")
        },
        complete: () => {
            console.log("se termino de procesar la información")
        },
        success: (respuesta) => {
            if (respuesta.estado == 'ok') {
                $('#nombreEditar').val(respuesta.mensaje[0].nombre)
                $('#passwordEditar').val(respuesta.mensaje[0].contrasenia)
                $('#tipoUsuarioEditar').val(respuesta.mensaje[0].tipo)

            }
            else {
                alert(respuesta.mensaje)
            }
        }
    });
}

var actualizarInfotmacion = (objetoUsuario) => {
    $.ajax({
        data: objetoUsuario,
        url: 'php/opciones-a-realizar.php',
        type: "post",
        beforeSend: () => {
            console.log("Procesando información")
        },
        complete: () => {
            console.log("se termino de procesar la información")
        },
        success: (respuesta) => {
            if (respuesta.estado == 'ok') {
                alert(respuesta.mensaje)
                cargarDatos();
            }
            else {
                alert(respuesta.mensaje)
            }
        }
    });
}
