<?php
    header('content-type: application/json; charset=utf-8');
    include_once '../php/class.conexion.php';
    include_once '../php/class.queries.php';
    
    $tipoDeAccion =  ( isset($_POST['tipo']) )  ? $_POST['tipo']  : 'no esta definida la variable'; 
    switch ($tipoDeAccion) {
        case 'cargarInformacion':
            $queries = new Queries();
            $datos = $queries->cargarInformacion();
            if( sizeof( $datos ) > 0 ){
                $respuesta['estado']="ok";
                $respuesta['mensaje']= $datos;
            }
            else{
                $respuesta['estado']="error";
                $respuesta['mensaje']= "no se pudo cargar la información";
            }
        break;
        case 'agregarInformacion':
                $queries = new Queries();
                $crearUsuario = $queries->crearUsuario( $_POST['usuario'] , $_POST['password'] ,  $_POST['tipoUsuario']);
                if(  $crearUsuario ){
                    $respuesta['estado']="ok";
                    $respuesta['mensaje']= "se ingreso el usuario correctamente";
                }
                else{
                    $respuesta['estado']="error";
                    $respuesta['mensaje']= "no se pudo agregar el usuario";
                }
        break;
        case 'eliminarUsuario':
                $queries = new Queries();
                $eliminarUsuario = $queries->eliminarUsuario( $_POST['idUsuario'] );
                if( $eliminarUsuario){
                    $respuesta['estado']="ok";
                    $respuesta['mensaje']= "se elimino el usuario correctamente";
                }
                else{
                    $respuesta['estado']="ok";
                    $respuesta['mensaje']= "no se pudo eliminar el usuario";
                }

        break;
        case 'informacionUsuario':
            $queries = new Queries();
            $informacionUsuario = $queries->informacionUsuario( $_POST['idUsuario'] );
            if( $informacionUsuario != null && sizeof( $informacionUsuario ) > 0 ){
                $respuesta['estado']="ok";
                $respuesta['mensaje']= $informacionUsuario;
            }
            else{
                $respuesta['estado']="error";
                $respuesta['mensaje']= "no se pudo cargar la información del usuario";
            }

        break;
        case 'guardarInformacion':
            $queries = new Queries();
            $editarUsuario = $queries->editarUsuario( $_POST['idUsuario'] , $_POST['usuario'] , $_POST['password'] , $_POST['tipoUsuario'] );
            if( $editarUsuario ){
                $respuesta['estado']="ok";
                $respuesta['mensaje']= "se edito el usuario correctamente";
            }
            else{
                $respuesta['estado']="ok";
                $respuesta['mensaje']= "no se pudo editar el usuario";
            }
        break;
    }

echo json_encode($respuesta);
?>